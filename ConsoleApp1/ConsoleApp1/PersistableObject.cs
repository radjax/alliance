﻿using ConsoleApp1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1
{
    public class PersistableObject
    {
        public string Id;

        public void Save()
        {
            string fileType = this.GetType().Name;
            string path = @"C:\test\" + fileType + ".json";

            bool fileExists = File.Exists(path);

            if(!fileExists)
            {

                using (StreamWriter file = File.CreateText(path))
                {
                    List<PersistableObject> emptyList = new List<PersistableObject>();
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, emptyList);
                }
            }

            using (StreamReader r = new StreamReader(path))
            {
                var json = r.ReadToEnd();
                var objects = JsonConvert.DeserializeObject<List<PersistableObject>>(json);
                r.Close();


                using (StreamWriter file = File.CreateText(path))
                {
                    this.Id = Guid.NewGuid().ToString();
                    List<PersistableObject> objectsToSave = objects ?? new List<PersistableObject>();
                    objectsToSave.Add(this);
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, objectsToSave);
                }
            }
        }

        public static dynamic Find(string id)
        {

            using (StreamReader r = new StreamReader(@"C:\test\Customer.json"))
            {
                var json = r.ReadToEnd();
                var customers = JsonConvert.DeserializeObject<List<Customer>>(json);
                r.Close();
                foreach(Customer customer in customers)
                {
                    if(customer.Id == id)
                    {
                        return customer;
                    }
                }
            }

            using (StreamReader r = new StreamReader(@"C:\test\Company.json"))
            {
                var json = r.ReadToEnd();
                var companies = JsonConvert.DeserializeObject<List<Company>>(json);
                r.Close();
                foreach (Company company in companies)
                {
                    if (company.Id == id)
                    {
                        return company;
                    }
                }
            }

            return null;
        }

        public void Delete()
        {
            string fileType = this.GetType().Name;

            using (StreamReader r = new StreamReader(@"C:\test\" + fileType + ".json"))
            {
                var json = r.ReadToEnd();
                var objects = JsonConvert.DeserializeObject<List<PersistableObject>>(json);
                r.Close();

                foreach (var item in objects)
                {
                    if(item.Id == this.Id)
                    {
                        objects.Remove(item);
                        this.Id = null;
                        break;
                    }
                }

                using (StreamWriter file = File.CreateText(@"C:\test\" + fileType + ".json"))
                {
                    List<PersistableObject> objectsToSave = objects ?? new List<PersistableObject>();
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, objectsToSave);
                }

            }
        }
    }
}

