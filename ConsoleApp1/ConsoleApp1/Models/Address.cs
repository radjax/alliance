﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Models
{
    public class Address : IEquatable<Address>
        {
        public string street;
        public string city;
        public string state;
        public string zip;

        public Address(string street, string city, string state, string zip)
        {
            this.street = street;
            this.city = city;
            this.state = state;
            this.zip = zip;
        }

        public bool Equals(Address other)
        {
            if (other == null)
            {
                return false;
            }
            return StringComparer.Ordinal.Equals(street, other.street);
        }
    }
}
