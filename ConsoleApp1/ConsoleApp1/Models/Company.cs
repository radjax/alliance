﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Models
{
    public class Company : PersistableObject, IEquatable<Company>
    {
        public string Name;
        public Address Address;

        public Company(string name, Address address)
        {
            this.Name = name;
            this.Address = address;
        }
        public bool Equals(Company other)
        {
            if (other == null)
            {
                return false;
            }
            return StringComparer.Ordinal.Equals(Name, other.Name);
        }
    }
}
