﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Models
{
    public class Customer : PersistableObject, IEquatable<Customer>
    {
        public string FirstName;
        public string LastName;
        public Address Address;

        public Customer(string firstName, string lastName, Address address)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
        }

        public bool Equals(Customer other)
        {
            if (other == null)
            {
                return false;
            }
            return StringComparer.Ordinal.Equals(FirstName, other.FirstName);
        }
    }
}
